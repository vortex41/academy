<?php

namespace Academy\Repository\Departments;

use Academy\Entity\Department;
use Academy\Repository\Departments;
use Ramsey\Uuid\UuidInterface;

class InMemory implements Departments
{
    /**
     * @var Department[]
     */
    private array $departments = [];

    public function get(UuidInterface $id): ?Department
    {
        return $this->departments[$id->toString()] ?? null;
    }

    public function save(Department $department): void
    {
        $this->departments[$department->getId()->toString()] = $department;
    }
}