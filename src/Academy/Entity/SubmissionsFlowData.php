<?php

namespace Academy\Entity;

class SubmissionsFlowData
{
    private bool $openForSubmissions = false;

    private int $capacity;

    private int $acceptedApplicationsCounter = 0;

    private SubmissionsFlowStatus $status;

    public function __construct()
    {
    }

    /**
     * @return bool
     */
    public function isOpenForSubmissions(): bool
    {
        return $this->openForSubmissions;
    }

    /**
     * @param bool $openForSubmissions
     */
    public function setOpenForSubmissions(bool $openForSubmissions): void
    {
        $this->openForSubmissions = $openForSubmissions;
    }

    /**
     * @return int
     */
    public function getCapacity(): int
    {
        return $this->capacity;
    }

    /**
     * @param int $capacity
     */
    public function setCapacity(int $capacity): void
    {
        $this->capacity = $capacity;
    }

    /**
     * @return int
     */
    public function getAcceptedApplicationsCounter(): int
    {
        return $this->acceptedApplicationsCounter;
    }

    /**
     * @param int $acceptedApplicationsCounter
     */
    public function setAcceptedApplicationsCounter(int $acceptedApplicationsCounter): void
    {
        $this->acceptedApplicationsCounter = $acceptedApplicationsCounter;
    }

    /**
     * @return SubmissionsFlowStatus
     */
    public function getStatus(): SubmissionsFlowStatus
    {
        return $this->status;
    }

    /**
     * @param SubmissionsFlowStatus $status
     */
    public function setStatus(SubmissionsFlowStatus $status): void
    {
        $this->status = $status;
    }
}