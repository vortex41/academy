<?php

namespace Academy\Entity;

enum Type : string
{
    case FLOW_STANDARD = 'flow_standard';
    case FLOW_SUPPLEMENTARY = 'flow_supplementary';
    case FLOW_CONTINUOUS = 'flow_continuous';
}