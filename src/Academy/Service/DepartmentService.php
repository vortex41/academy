<?php

namespace Academy\Service;

use Academy\Entity\Department;
use Academy\Entity\SubmissionsFlowData;
use Academy\Entity\SubmissionsFlowStatus;
use Academy\Entity\Type;
use Academy\Repository\Departments;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class DepartmentService
{
    private Departments $departments;

    /**
     * @param Departments $departments
     */
    public function __construct(Departments $departments)
    {
        $this->departments = $departments;
    }

    public function createDepartment(string $name, string $description): UuidInterface
    {
        $department = new Department();

        $department->setId(Uuid::uuid4());
        $department->setName($name);
        $department->setDescription($description);
        $department->setSubmissionsFlowData([]);

        $this->departments->save($department);

        return $department->getId();
    }

    public function changeDepartmentDescription(UuidInterface $departmentId, string $name, string $description): void
    {
        $department = $this->departments->get($departmentId);

        if ($department !== null) {
            $department->setName($name);
            $department->setDescription($description);

            $this->departments->save($department);
        } else {
            throw new \Exception('Department does not exist');
        }
    }

    public function prepareForSubmissions(UuidInterface $departmentId, int $year, Type $type, int $capacity): void
    {
        $department = $this->departments->get($departmentId);

        if ($department !== null) {
            $data = $department->getSubmissionsFlowData();

            if (!array_key_exists($year . '-' . $type->value, $data)) {
                $submissionsFlowData = new SubmissionsFlowData();

                $submissionsFlowData->setOpenForSubmissions(false);
                $submissionsFlowData->setCapacity($capacity);
                $submissionsFlowData->setAcceptedApplicationsCounter(0);
                $submissionsFlowData->setStatus(SubmissionsFlowStatus::DRAFT);

                $data[$year . '-' . $type->value] = $submissionsFlowData;

                $department->setSubmissionsFlowData($data);
            } else {
                throw new \Exception('Data already prepared for submissions');
            }

            $this->departments->save($department);
        } else {
            throw new \Exception('Department does not exist');
        }
    }

    public function startAcceptApplications(UuidInterface $departmentId, int $year, Type $type): void
    {
        $department = $this->departments->get($departmentId);

        if ($department !== null) {
            $data = $department->getSubmissionsFlowData();

            if (array_key_exists($year . '-' . $type->value, $data)) {
                $submissionsFlowData = $data[$year . '-' . $type->value];

                $submissionsFlowData->setStatus(SubmissionsFlowStatus::OPEN);
                $submissionsFlowData->setOpenForSubmissions(true);

                $department->setSubmissionsFlowData($data);
            } else {
                throw new \Exception('Accepting submissions was not prepared yet');
            }

            $this->departments->save($department);
        } else {
            throw new \Exception('Department does not exist');
        }
    }

    public function stopAcceptApplications(UuidInterface $departmentId, int $year, Type $type): void
    {
        $department = $this->departments->get($departmentId);

        if ($department !== null) {
            $data = $department->getSubmissionsFlowData();

            if (array_key_exists($year . '-' . $type->value, $data)) {
                $submissionsFlowData = $data[$year . '-' . $type->value];

                $submissionsFlowData->setStatus(SubmissionsFlowStatus::CLOSED);
                $submissionsFlowData->setOpenForSubmissions(false);

                $department->setSubmissionsFlowData($data);
            } else {
                throw new \Exception('Accepting submissions was not prepared yet');
            }
        } else {
            throw new \Exception('Department does not exist');
        }
    }

    public function setCapacity(UuidInterface $departmentId, int $year, Type $type, $capacity): void
    {
        $department = $this->departments->get($departmentId);

        if ($department !== null) {
            $data = $department->getSubmissionsFlowData();
            $currentCapacity = $data[$year . '-' . $type->value]->getCapacity();

            if ($currentCapacity > $capacity) {
                $data[$year . '-' . $type->value]->setCapacity($capacity);

                $department->setSubmissionsFlowData($data);
            } else {
                throw new \Exception('New capacity cannot be lower then current, due to law regulations');
            }

            $this->departments->save($department);
        } else {
            throw new \Exception('Department does not exist');
        }
    }
}