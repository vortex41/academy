<?php

namespace Academy\Service;

use Academy\Entity\Application;
use Academy\Entity\ApplicationStatus;
use Academy\Entity\ExamResult;
use Academy\Entity\IdentityNumberType;
use Academy\Entity\Student;
use Academy\Entity\StudentStatus;
use Academy\Entity\Type;
use Academy\Repository\Applications;
use Academy\Repository\Departments;
use Academy\Repository\Students;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class ApplicationService
{
    private NotificationService $notifications;

    private Applications $applications;

    private Students $students;

    private Departments $departments;

    /**
     * @param NotificationService $notifications
     * @param Applications $applications
     * @param Students $students
     * @param Departments $departments
     */
    public function __construct(NotificationService $notifications, Applications $applications, Students $students, Departments $departments)
    {
        $this->notifications = $notifications;
        $this->applications = $applications;
        $this->students = $students;
        $this->departments = $departments;
    }

    public function createApplication(
        string             $firstName,
        string             $lastName,
        IdentityNumberType $numberType,
        string             $identityNumber,
        string             $email,
        array              $examResults
    ): UuidInterface
    {
        $student = $this->students->findBy($numberType, $identityNumber);

        if ($student === null) {
            $student = new Student();
            $student->setId(Uuid::uuid4());
            $student->setFirstName($firstName);
            $student->setLastName($lastName);
            $student->setStatus(StudentStatus::CANDIDATE);
            $student->setEmail($email);
            $student->setCreatedAt(new \DateTimeImmutable());
            $student->setIdentityNumber($identityNumber);
            $student->setNumberType($numberType);

            $this->students->save($student);
        } else {
            $student->setFirstName($firstName);
            $student->setLastName($lastName);
            $student->setEmail($email);
        }
        $application = new Application();
        $application->setId(Uuid::uuid4());
        $application->setCreatedAt(new \DateTimeImmutable());
        $application->setStatus(ApplicationStatus::DRAFT);
        $totalScore = 0;

        foreach ($examResults as $result) {
            if ($result->getSubject() === '') {
                throw new \Exception('Invalid subject');
            }

            if ($result->getResult() < 1 || $result->getResult() > 6) {
                throw new \Exception('Exam result out of scope');
            }

            $totalScore += $result->getResult();
        }

        $application->setExamResults($examResults);
        $application->setStudentId($student->getId());
        $application->setAverageScore($totalScore / count($application->getExamResults()));
        $this->applications->save($application);
        return $application->getId();
    }

    public function updateApplication(UuidInterface $applicationId, array $examResults): void
    {
        $application = $this->applications->get($applicationId);
        if ($application !== null) {
            if ($application->getStatus() === ApplicationStatus::DRAFT) {
                $application->setExamResults($examResults);
                $totalScore = 0;
                foreach ($examResults as $result) {
                    if (in_array($result->getResult(), [1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0])) {
                        $totalScore += $result->getResult();
                    } else {
                        throw new \Exception('Exam result out of scope');
                    }
                    if ($result->getSubject() === '') {
                        throw new \Exception('Invalid subject');
                    }
                }
                $application->setAverageScore($totalScore / count($application->getExamResults()));
                $this->applications->save($application);
            } else {
                throw new \Exception('Application subbmitted and cannot be modified');
            }
        } else {
            throw new
            \Exception('Application doesn exist');
        }
    }

    public function submitApplication(
        UuidInterface $applicationId,
        UuidInterface $departmentId,
        int           $year,
        Type          $type
    ): bool
    {

        $department = $this->departments->get($departmentId);

        if ($department !== null) {
            $application = $this->applications->get($applicationId);

            if ($application !== null) {
                // @todo Dodać wsparcie dla duplikatów różnych aplikacji tego samego kandydata, bo jest to problem!
                if ($application->getStatus() === ApplicationStatus::DRAFT) {
                    // logika sprawdzania, czy można przyjąć zgłoszenie

                    if ($department->canAcceptApplicationsFor($year, $type)) {
                        // sprawdzenie limitu zaakceptowanych zgłoszen

                        if ($department->getName() != 'Fizyka uzupelniajaca') {
                            if ($type === Type::FLOW_STANDARD) {
                                $now = new \DateTimeImmutable();

                                if ($now->format('m') >= 6 && $now->format('m' <= 8)) {
                                    $application->setType($type);
                                } else {
                                    throw new \Exception('Application cannot be submitted, not a right moment!');
                                }
                            } else {
                                $now = new \DateTimeImmutable();

                                if ($now->format('m') >= 1 && $now->format('m') <= 2) {
                                    $application->setType($type);
                                } else {
                                    throw new \Exception('Application cannot be submitted, not a right moment!');
                                }
                            }

                            if ($department->getAcceptedApplicationsCounter($year, $type) >= $department->getCapacity($year, $type)) {
                                throw new \Exception('Application cannot be submitted, max capacity reached');
                            }
                        } else {
                            $application->setType(Type::FLOW_CONTINUOUS);
                        }

                        $application->setSubmittedAt($now);
                        $application->setStatus(ApplicationStatus::SUBMITTED);
                        $application->setDepartmentId($departmentId);
                        $application->setYear($year);

                        return true;
                    } else {
                        throw new \Exception('Department does not accept applications now');
                    }
                } else {
                    throw new \Exception("Application already submitted");
                }
            } else {
                throw new \Exception("Application with given id does not exist");
            }
        } else {
            throw new \Exception("Department with given id does not exist");
        }
    }

    public function acceptApplication(UuidInterface $applicationId): bool
    {
        $application = $this->applications->get($applicationId);
        $department = $this->departments->get(
            $application->getDepartmentId()
        );

        if ($application !== null) {

            if ($application->getStatus() !== ApplicationStatus::SUBMITTED) {
                throw new \Exception('Application already processed');
            }

            if ($department->getAcceptedApplicationsCounter($application->getYear(),$application->getType())>=$department->getCapacity($application->getYear(),$application->getType())) {
                throw new \Exception('Application cannot be accepted, max capacity reached');
            }

            $application->setStatus(ApplicationStatus::ACCEPTED);
            $application->setProcessedAt(new \DateTimeImmutable());

            $department->setAcceptedApplicationsCounter($application->getYear(),$application->getType(),$department->getAcceptedApplicationsCounter($application->getYear(),$application->getType())+1
            );

            $this->applications->save($application);
            $this->departments->save($department);

            return true;
        } else {
            throw new \Exception('Application doesn exist');
        }
    }

    public function rejectApplication(UuidInterface $applicationId): bool
    {
        $application = $this->applications->get($applicationId);

        if ($application !== null) {
            if ($application->getStatus() === ApplicationStatus::SUBMITTED) {

                $application->setProcessedAt(new \DateTimeImmutable());
                $application->setStatus(ApplicationStatus::REJECTED);
            } else
                if ($application->getStatus()===ApplicationStatus::ACCEPTED) {
                $department = $this->departments->get($application->getDepartmentId());
                $application->setProcessedAt(new \DateTimeImmutable());
                $department->setAcceptedApplicationsCounter(
                    $application->getYear(),
                    $application->getType(),
                    $department->getAcceptedApplicationsCounter($application->getYear(), $application->getType()) - 1
                );
                $application->setStatus(ApplicationStatus::REJECTED);
                $this->departments->save($department);
            } else {
                throw new \Exception('Application cannot be rejected');
            }

            $this->applications->save($application);

            return true;


        } else {
            throw new \Exception('Application doesn exist');
        }
    }
}